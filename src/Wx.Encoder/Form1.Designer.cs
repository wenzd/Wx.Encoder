﻿namespace Wx.Encoder
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUrlEncode = new System.Windows.Forms.TextBox();
            this.txtHtmlDecode = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtUrlDecode = new System.Windows.Forms.TextBox();
            this.txtHtmlEncode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cboEncoding = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtOriginal = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCpyUrlEncode = new System.Windows.Forms.Button();
            this.btnCpyUrlDecode = new System.Windows.Forms.Button();
            this.btnCpyHtmlEncode = new System.Windows.Forms.Button();
            this.btnCpyHtmlDecode = new System.Windows.Forms.Button();
            this.btnPasteOriginal = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1342, 202);
            this.panel1.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnCpyHtmlDecode);
            this.groupBox1.Controls.Add(this.btnCpyHtmlEncode);
            this.groupBox1.Controls.Add(this.btnCpyUrlDecode);
            this.groupBox1.Controls.Add(this.btnCpyUrlEncode);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtUrlEncode);
            this.groupBox1.Controls.Add(this.txtHtmlDecode);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtUrlDecode);
            this.groupBox1.Controls.Add(this.txtHtmlEncode);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1342, 128);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Result";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "UrlEncode:";
            // 
            // txtUrlEncode
            // 
            this.txtUrlEncode.Location = new System.Drawing.Point(87, 21);
            this.txtUrlEncode.Name = "txtUrlEncode";
            this.txtUrlEncode.ReadOnly = true;
            this.txtUrlEncode.Size = new System.Drawing.Size(1167, 21);
            this.txtUrlEncode.TabIndex = 3;
            // 
            // txtHtmlDecode
            // 
            this.txtHtmlDecode.Location = new System.Drawing.Point(87, 102);
            this.txtHtmlDecode.Name = "txtHtmlDecode";
            this.txtHtmlDecode.ReadOnly = true;
            this.txtHtmlDecode.Size = new System.Drawing.Size(1167, 21);
            this.txtHtmlDecode.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "UrlDecode:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "HtmlDecode:";
            // 
            // txtUrlDecode
            // 
            this.txtUrlDecode.Location = new System.Drawing.Point(87, 48);
            this.txtUrlDecode.Name = "txtUrlDecode";
            this.txtUrlDecode.ReadOnly = true;
            this.txtUrlDecode.Size = new System.Drawing.Size(1167, 21);
            this.txtUrlDecode.TabIndex = 5;
            // 
            // txtHtmlEncode
            // 
            this.txtHtmlEncode.Location = new System.Drawing.Point(87, 75);
            this.txtHtmlEncode.Name = "txtHtmlEncode";
            this.txtHtmlEncode.ReadOnly = true;
            this.txtHtmlEncode.Size = new System.Drawing.Size(1167, 21);
            this.txtHtmlEncode.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 12);
            this.label4.TabIndex = 6;
            this.label4.Text = "HtmlEncode:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnPasteOriginal);
            this.panel2.Controls.Add(this.cboEncoding);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtOriginal);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1342, 74);
            this.panel2.TabIndex = 10;
            // 
            // cboEncoding
            // 
            this.cboEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEncoding.FormattingEnabled = true;
            this.cboEncoding.Location = new System.Drawing.Point(89, 40);
            this.cboEncoding.Name = "cboEncoding";
            this.cboEncoding.Size = new System.Drawing.Size(93, 20);
            this.cboEncoding.TabIndex = 3;
            this.cboEncoding.SelectedValueChanged += new System.EventHandler(this.cboEncoding_SelectedValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(19, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 2;
            this.label6.Text = "Encoding:";
            // 
            // txtOriginal
            // 
            this.txtOriginal.Location = new System.Drawing.Point(89, 13);
            this.txtOriginal.Name = "txtOriginal";
            this.txtOriginal.Size = new System.Drawing.Size(1165, 21);
            this.txtOriginal.TabIndex = 1;
            this.txtOriginal.TextChanged += new System.EventHandler(this.txtOriginal_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "Original:";
            // 
            // btnCpyUrlEncode
            // 
            this.btnCpyUrlEncode.Location = new System.Drawing.Point(1260, 19);
            this.btnCpyUrlEncode.Name = "btnCpyUrlEncode";
            this.btnCpyUrlEncode.Size = new System.Drawing.Size(75, 23);
            this.btnCpyUrlEncode.TabIndex = 10;
            this.btnCpyUrlEncode.Text = "Copy";
            this.btnCpyUrlEncode.UseVisualStyleBackColor = true;
            this.btnCpyUrlEncode.Click += new System.EventHandler(this.btnCpyUrlEncode_Click);
            // 
            // btnCpyUrlDecode
            // 
            this.btnCpyUrlDecode.Location = new System.Drawing.Point(1260, 46);
            this.btnCpyUrlDecode.Name = "btnCpyUrlDecode";
            this.btnCpyUrlDecode.Size = new System.Drawing.Size(75, 23);
            this.btnCpyUrlDecode.TabIndex = 11;
            this.btnCpyUrlDecode.Text = "Copy";
            this.btnCpyUrlDecode.UseVisualStyleBackColor = true;
            this.btnCpyUrlDecode.Click += new System.EventHandler(this.btnCpyUrlDecode_Click);
            // 
            // btnCpyHtmlEncode
            // 
            this.btnCpyHtmlEncode.Location = new System.Drawing.Point(1260, 73);
            this.btnCpyHtmlEncode.Name = "btnCpyHtmlEncode";
            this.btnCpyHtmlEncode.Size = new System.Drawing.Size(75, 23);
            this.btnCpyHtmlEncode.TabIndex = 12;
            this.btnCpyHtmlEncode.Text = "Copy";
            this.btnCpyHtmlEncode.UseVisualStyleBackColor = true;
            this.btnCpyHtmlEncode.Click += new System.EventHandler(this.btnCpyHtmlEncode_Click);
            // 
            // btnCpyHtmlDecode
            // 
            this.btnCpyHtmlDecode.Location = new System.Drawing.Point(1260, 100);
            this.btnCpyHtmlDecode.Name = "btnCpyHtmlDecode";
            this.btnCpyHtmlDecode.Size = new System.Drawing.Size(75, 23);
            this.btnCpyHtmlDecode.TabIndex = 13;
            this.btnCpyHtmlDecode.Text = "Copy";
            this.btnCpyHtmlDecode.UseVisualStyleBackColor = true;
            this.btnCpyHtmlDecode.Click += new System.EventHandler(this.btnCpyHtmlDecode_Click);
            // 
            // btnPasteOriginal
            // 
            this.btnPasteOriginal.Location = new System.Drawing.Point(1260, 11);
            this.btnPasteOriginal.Name = "btnPasteOriginal";
            this.btnPasteOriginal.Size = new System.Drawing.Size(75, 23);
            this.btnPasteOriginal.TabIndex = 4;
            this.btnPasteOriginal.Text = "Paste";
            this.btnPasteOriginal.UseVisualStyleBackColor = true;
            this.btnPasteOriginal.Click += new System.EventHandler(this.btnPasteOriginal_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1342, 202);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Wx.Encode v1.0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtUrlEncode;
        private System.Windows.Forms.TextBox txtHtmlDecode;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUrlDecode;
        private System.Windows.Forms.TextBox txtHtmlEncode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtOriginal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboEncoding;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCpyHtmlDecode;
        private System.Windows.Forms.Button btnCpyHtmlEncode;
        private System.Windows.Forms.Button btnCpyUrlDecode;
        private System.Windows.Forms.Button btnCpyUrlEncode;
        private System.Windows.Forms.Button btnPasteOriginal;
    }
}

