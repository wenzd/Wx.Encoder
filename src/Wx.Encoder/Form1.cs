﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using System.Collections.Specialized;
using Wx.Encoder.Entities;
using Wx.Encoder.Enums;

namespace Wx.Encoder
{
    public partial class Form1 : Form
    {
        Dictionary<Format, TextBox> dicTextBox;

        public Form1()
        {
            InitializeComponent();

            dicTextBox = new Dictionary<Format, TextBox>()
            {
                { Format.UrlEncode, txtUrlEncode },
                { Format.UrlDecode, txtUrlDecode },
                { Format.HtmlEncode, txtHtmlEncode },
                { Format.HtmlDecode, txtHtmlDecode },
            };
        }

        private void txtOriginal_TextChanged(object sender, EventArgs e)
        {
            RefreshControls();
        }

        void RefreshControls()
        {
            string sOriginal = txtOriginal.Text;
            Encoding eEncoding = ((NameValue)cboEncoding.SelectedItem).Value as Encoding;

            txtUrlEncode.Text = HttpUtility.UrlEncode(sOriginal, eEncoding);
            txtUrlDecode.Text = HttpUtility.UrlDecode(sOriginal, eEncoding);
            txtHtmlEncode.Text = HttpUtility.HtmlEncode(sOriginal);
            txtHtmlDecode.Text = HttpUtility.HtmlDecode(sOriginal);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            List<NameValue> list = new List<NameValue>();
            list.Add(new NameValue() { Name = "UTF8", Value = Encoding.UTF8, });
            list.Add(new NameValue() { Name = "Unicode", Value = Encoding.Unicode, });
            list.Add(new NameValue() { Name = "ASCII", Value = Encoding.ASCII, });
            list.Add(new NameValue() { Name = "GBK", Value = Encoding.GetEncoding("GBK"), });
            list.Add(new NameValue() { Name = "GB2312", Value = Encoding.GetEncoding("GB2312"), });

            cboEncoding.DataSource = list;
            cboEncoding.DisplayMember = "Name";
            cboEncoding.ValueMember = "Value";
        }

        private void cboEncoding_SelectedValueChanged(object sender, EventArgs e)
        {
            RefreshControls();
        }

        void SetClipboard(Format format)
        {
            Clipboard.SetData(DataFormats.Text, dicTextBox[format].Text);
        }

        private void btnCpyUrlEncode_Click(object sender, EventArgs e)
        {
            SetClipboard(Format.UrlEncode);
        }

        private void btnCpyUrlDecode_Click(object sender, EventArgs e)
        {
            SetClipboard(Format.UrlDecode);
        }

        private void btnCpyHtmlEncode_Click(object sender, EventArgs e)
        {
            SetClipboard(Format.HtmlEncode);
        }

        private void btnCpyHtmlDecode_Click(object sender, EventArgs e)
        {
            SetClipboard(Format.HtmlDecode);
        }

        private void btnPasteOriginal_Click(object sender, EventArgs e)
        {
            txtOriginal.Text = Clipboard.GetText();
        }
    }
}
