﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wx.Encoder.Entities
{
    public class NameValue
    {
        public string Name { get; set; }
        public object Value { get; set; }
    }
}
