﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wx.Encoder.Enums
{
    public enum Format
    {
        UrlEncode,
        UrlDecode,
        HtmlEncode,
        HtmlDecode,
    }
}
